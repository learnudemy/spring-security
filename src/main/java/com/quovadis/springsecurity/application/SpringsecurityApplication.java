package com.quovadis.springsecurity.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScans({@ComponentScan("com.quovadis.springsecurity.controller"),
		@ComponentScan("com.quovadis.springsecurity.config")})
@EnableJpaRepositories("com.quovadis.springsecurity.repository")
@EntityScan("com.quovadis.springsecurity.model")
public class SpringsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringsecurityApplication.class, args);
	}

}
